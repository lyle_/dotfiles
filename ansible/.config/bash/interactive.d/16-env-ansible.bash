################################################################################
# env-ansible.bash - environment overrides for Ansible
################################################################################

export ANSIBLE_ROLES_PATH="$XDG_DATA_HOME/ansible/roles"
