################################################################################
# third-party.bash - setup for third-party toolchains
################################################################################

# RVM - https://rvm.io/
[[ -s "$HOME/.rvm/scripts/rvm" ]] && \
	# Load RVM into a shell session *as a function*
	source "$HOME/.rvm/scripts/rvm"
