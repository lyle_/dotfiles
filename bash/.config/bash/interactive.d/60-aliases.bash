################################################################################
# aliases.bash - aliases for interactive commands
################################################################################

# Handy aliases for editing configuration files. These correspond to <leader>
# mappings in vim.
# Mnemonic: 'e' for edit
################################################################################
alias ebrc='vi ~/.bashrc; . ~/.bashrc'
alias ebp='vi ~/.bash_profile; . ~/.bash_profile'
alias ef='vi $XDG_CONFIG_HOME/fontconfig/fonts.conf'
alias ei='vi $XDG_CONFIG_HOME/i3/config'
alias en='vi $XDG_CONFIG_HOME/nvim/init.vim'
alias ep='vi $XDG_CONFIG_HOME/polybar/config'
alias er='vi $DOTFILES_DIR/README.md'
alias es='vi $XDG_CONFIG_HOME/sway/config'
alias et='vi $DROPBOX_DIR/Documents/TODO.md'
alias evp='vi $XDG_CONFIG_HOME/vim/plugins.vim'
alias evs='vi $XDG_CONFIG_HOME/vim/settings.vim'


# Handy aliases for navigating the filesystem
# Mnemonic: 'g' for go
################################################################################
alias gd='cd $DOTFILES_DIR'
alias gs='cd ~/src'
alias gss='cd ~/src/sandbox'
alias gw='cd $DROPBOX_DIR/vimwiki'

# Follow dmesg output
alias dmf='dmesg --follow'

# Git
################################################################################
alias g=git
# Common Git workflows
function merge() {
    git checkout master && git pull && git branch -d "$1"
}

# Text editing
################################################################################
if type nvim >/dev/null 2>&01; then
	alias vi=nvim
else
	alias vi=vim
fi

# Use interactive versions of destructive operations
################################################################################
alias rm='rm -i'
alias mv='mv -i'
alias cp='cp -i'

# Sysadmin housekeeping
################################################################################
# Show memory hogs
alias mem='ps -e -orss=,pid=,args= | sort -b -k1,1n | pr -TW$COLUMNS'
# Show external IP address
alias whatsmyip='printf "$(curl -s https://checkip.amazonaws.com)\n"'

# Docker
################################################################################
# Remove exited docker containers
alias drmc='docker rm $(docker ps -q -f status=exited)'
# Remove unused docker images
alias drmi="docker images | grep \" [months|days|weeks]* ago\" | awk \"{print $3}\" | xargs docker rmi"
# 'docker stats' show container name instead of ID (pending fix of https://github.com/docker/docker/issues/20973)
alias ds='while true; do TEXT=$(docker stats --no-stream $(docker ps --format={{.Names}})); sleep 0.1; clear; echo "$TEXT"; done'

# Services used often for local development
################################################################################
alias activemq="docker run --name='activemq' --interactive --rm -p 8161:8161 -p 61616:61616 -p 61613:61613 webcenter/activemq"
alias graphite="docker run --name graphite   --interactive --rm -p 80:80 -p 2003-2004:2003-2004 -p 2023-2024:2023-2024 -p 8125:8125/udp -p 8126:8126 hopsoft/graphite-statsd"
