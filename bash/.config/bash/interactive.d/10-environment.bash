################################################################################
# environment.bash - configuration setting environment variables
################################################################################

# Add local programs and scripts
export PATH=$HOME/.local/bin:$PATH

# Directory where dotfiles are managed with stow
export DOTFILES_DIR=~/.dotfiles
export DROPBOX_DIR=~/drop

# XDG: see https://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html
# Explicitly set XDG locations for software which respects the variable
# but doesn't follow the spec as far as defaults.
export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share
if [[ -d /run/user/$(id -u) ]]; then
	# Looks like systemd is in play
	export XDG_RUNTIME_DIR=/run/user/$(id -u)
elif [[ -d $TMPDIR ]]; then
	export XDG_RUNTIME_DIR=$TMPDIR
else
	export XDG_RUNTIME_DIR=/tmp/$(id -u)
fi
# This isn't defined by the spec, but I think it probably should be.
export XDG_LIB_HOME=$HOME/.local/lib


# Set default editor
if type nvim >/dev/null 2>&01; then
	export VISUAL=nvim
elif type vim >/dev/null 2>&01; then
	export VISUAL=vim
else
	export VISUAL=vi
fi
export EDITOR="$VISUAL"


# Set default web browser
if [ -n "$DISPLAY" ]; then
	export BROWSER=firefox
else
	export BROWSER=w3m
fi


# Set SDKMAN! directory
# SDK Man replaced GVM. Using for Groovy, Gradle, and Maven Version Management
export SDKMAN_DIR="$XDG_DATA_HOME/sdkman"
[[ -s "$SDKMAN_DIR/bin/sdkman-init.sh" ]] && source "$SDKMAN_DIR/bin/sdkman-init.sh"


# Command history
################################################################################
# When the shell exits, append to the history file instead of overwriting it
shopt -s histappend
# Store this many commands in memory
export HISTSIZE=1000
# Store this many commands on disk
export HISTSIZE=5000
# Store more history, ignoring dupes
export HISTCONTROL=ignoredups:erasedups
# Put the history file somewhere XDG-friendly
export HISTFILE=$XDG_CACHE_HOME/bash_history
# less history also gets stowed
export LESSHISTFILE=$XDG_CACHE_HOME/less_history
