################################################################################
# .bashrc
################################################################################

if [[ $- != *i* ]] ; then
	# shell is non-interactive. be done now!
	return
fi

# Load all files from our interactive session config directory
if [ -d $HOME/.config/bash/interactive.d ]; then
	for file in $HOME/.config/bash/interactive.d/*.bash; do
		source $file
	done
fi

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/home/ldh/.sdkman"
[[ -s "/home/ldh/.sdkman/bin/sdkman-init.sh" ]] && source "/home/ldh/.sdkman/bin/sdkman-init.sh"
