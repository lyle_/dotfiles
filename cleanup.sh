#!/bin/sh
# Things which have tended to get in the way of a dotfiles update
cd ~
rm -fr .ackrc .bash* bin .cache .config .emacs .kshrc .gvimrc .v8flags* .m2 .lesshst .npm .oracle_jre_usage .screenrc  .vim* .zshrc .dotfiles/stow
