################################################################################
# env-firefox.bash - variables for firefox
################################################################################

# Run Firefox as a native Wayland application
export MOZ_ENABLE_WAYLAND=1
# https://mastransky.wordpress.com/2020/03/16/wayland-x11-how-to-run-firefox-in-mixed-environment/
export MOZ_DBUS_REMOTE=1
