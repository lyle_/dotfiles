################################################################################
# platformio.bash - Configure values for platformio
################################################################################

# Enable bash autocompletion
eval "$(_PLATFORMIO_COMPLETE=source platformio)"
eval "$(_PIO_COMPLETE=source pio)"
