################################################################################
# env-intellij.bash - environment variables for IntelliJ IDEA
################################################################################

export IDEA_PROPERTIES=${XDG_CONFIG_HOME}/intellij/idea.properties
export IDEA_VM_OPTIONS=${XDG_CONFIG_HOME}/intellij/idea64.vmoptions
