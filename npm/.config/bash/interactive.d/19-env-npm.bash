################################################################################
# env-npm.bash - environment variables for node package manager
################################################################################

node_modules_dir="${XDG_LIB_HOME}/node_modules"
PATH="${node_modules_dir}/bin:$PATH"
export npm_config_prefix="$node_modules_dir"
