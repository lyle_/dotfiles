################################################################################
# sway.bash - Run swaywm at login on VT1
################################################################################

if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
	logfile="$XDG_CACHE_HOME/sway/$(date +"%Y-%m-%d").log"
	mkdir -p "$(dirname "$logfile")"
	echo "Starting up at $(date)" >> "$logfile"
	XKB_DEFAULT_LAYOUT=us exec sway --debug >> "$logfile" 2>&1
fi
