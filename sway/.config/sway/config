# Read `man 5 sway` for a complete reference.

include "$XDG_CACHE_HOME/wal/colors-sway"

### Variables
################################################################################
# Logo key. Use Mod1 for Alt.
set $mod Mod1

# Home row direction keys, like vim
set $left h
set $down j
set $up k
set $right l

# Terminal emulator
set $term termite

# Application launcher
set $menu exec termite --name=floating -e "bash -c 'sway-launcher'"

# Lock screen
set $lockscreen swaylock --color 000000


### Startup programs
################################################################################

# Because we auto-login and boot right to sway, start with the lock screen
exec $lockscreen
# Notification service
exec mako
# Track window focus so we can activate the previous one when
# using a floating fzf prompt to take screenshots
exec focus-last
# Start keyring daemon
exec /usr/bin/gnome-keyring-daemon --start --components=pkcs11,secrets,ssh
# Start redshift
exec redshift

# Appearance
################################################################################
# Window titles (and bar, unless specified in bar{} below)
# Resolved (hopefully) via fontconfig (see $XDG_CONFIG_HOME/fontconfig/fonts.conf)
font pango:serif 10
# Suppress window titles, small border around active window
default_border pixel 4
# Windows with the "floating" id are...floating
for_window [app_id="^floating$"] floating enable, border none
floating_minimum_size 300 x 200
# Theme colors     border  backgr.  text      indicator  child_border
client.focused     $color4 $color4  $color15  $color13   $color4
client.unfocused   $color8 $color8  $color15  $color8    $color8
# Defaults:
# client.focused   #4c7899 #285577 #ffffff #2e9ef4    #285577
# client.unfocused #222222 #222222 $color1  #222222 $color1
# client.urgent    #274D01 #900000 #FFFFFF #900000 $color1

### Idle configuration
################################################################################
# Blank the screen after 30 minutes, locking beforehand
# 1800 = 30 minutes
exec swayidle \
    timeout 1800 'swaymsg "output * dpms off"' \
       resume 'swaymsg "output * dpms on"' \
    before-sleep '$lockscreen'
# xwayland version
for_window [class="Firefox"] inhibit_idle fullscreen
# wayland vesion
for_window [app_id="firefox"] inhibit_idle fullscreen


### Input configuration
################################################################################
# You can get the names of your inputs by running: swaymsg -t get_inputs
# Read `man 5 sway-input` for more information about this section.
input "*" {
	natural_scroll enabled
	repeat_delay 300
	repeat_rate 50
}


### Key bindings
################################################################################
#
# Basics:
#
    # start a terminal
    bindsym $mod+Return exec $term

	# Lock the screen
	bindsym $mod+Shift+x exec $lockscreen

    # kill focused window
    bindsym $mod+Shift+q kill

    # start your launcher
    bindsym $mod+d exec $menu

	# Take a screenshot
	bindsym $mod+Grave exec screenshot

	# Close notification
	bindsym $mod+Backspace exec makoctl dismiss
	# Close all notifications
	bindsym $mod+Backslash exec makoctl dismiss --all
	# Test notifications
	bindsym $mod+t exec notify-send "$(date)"

	# Show help
	bindsym $mod+Question exec termite --name=floating -e "bash 'cheatsheet'"

    # Drag floating windows by holding down $mod and left mouse button.
    # Resize them with right mouse button + $mod.
    # Despite the name, also works for non-floating windows.
    # Change normal to inverse to use left mouse button for resizing and right
    # mouse button for dragging.
    floating_modifier $mod normal

    # reload the configuration file
    bindsym $mod+Shift+c reload

    # exit sway (logs you out of your Wayland session)
    bindsym $mod+Shift+e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'

	# Audio controls
	bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume 0 +5% #increase sound volume
	bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume 0 -5% #decrease sound volume
	bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 0 toggle # mute sound
	## WIP TODO
	# volume-pulseaudiochange volume or toggle mute
	#bindsym XF86AudioRaiseVolume exec amixer -q -D pulse sset Master 5%+ && pkill -RTMIN+1 i3blocks
	#bindsym XF86AudioLowerVolume exec amixer -q -D pulse sset Master 5%- && pkill -RTMIN+1 i3blocks
	#bindsym XF86AudioMute exec amixer -q -D pulse sset Master toggle && pkill -RTMIN+1 i3blocks

	# Screen backlight controls
	bindsym XF86MonBrightnessUp exec light -A 5 # increase screen brightness
	bindsym XF86MonBrightnessDown exec light -U 5 # decrease screen brightness

#
# Moving around:
#
    # Move your focus around
    bindsym $mod+$left focus left
    bindsym $mod+$down focus down
    bindsym $mod+$up focus up
    bindsym $mod+$right focus right
    # or use $mod+[up|down|left|right]
    bindsym $mod+Left focus left
    bindsym $mod+Down focus down
    bindsym $mod+Up focus up
    bindsym $mod+Right focus right

    # _move_ the focused window with the same, but add Shift
    bindsym $mod+Shift+$left move left
    bindsym $mod+Shift+$down move down
    bindsym $mod+Shift+$up move up
    bindsym $mod+Shift+$right move right
    # ditto, with arrow keys
    bindsym $mod+Shift+Left move left
    bindsym $mod+Shift+Down move down
    bindsym $mod+Shift+Up move up
    bindsym $mod+Shift+Right move right
#
# Workspaces:
#
    # switch to workspace
    bindsym $mod+1 workspace 1
    bindsym $mod+2 workspace 2
    bindsym $mod+3 workspace 3
    bindsym $mod+4 workspace 4
    bindsym $mod+5 workspace 5
    bindsym $mod+6 workspace 6
    bindsym $mod+7 workspace 7
    bindsym $mod+8 workspace 8
    bindsym $mod+9 workspace 9
    bindsym $mod+0 workspace 10
    # move focused container to workspace
    bindsym $mod+Shift+1 move container to workspace 1
    bindsym $mod+Shift+2 move container to workspace 2
    bindsym $mod+Shift+3 move container to workspace 3
    bindsym $mod+Shift+4 move container to workspace 4
    bindsym $mod+Shift+5 move container to workspace 5
    bindsym $mod+Shift+6 move container to workspace 6
    bindsym $mod+Shift+7 move container to workspace 7
    bindsym $mod+Shift+8 move container to workspace 8
    bindsym $mod+Shift+9 move container to workspace 9
    bindsym $mod+Shift+0 move container to workspace 10
    # Note: workspaces can have any name you want, not just numbers.
    # We just use 1-10 as the default.
#
# Layout stuff:
#
    # You can "split" the current object of your focus with
    # $mod+b or $mod+v, for horizontal and vertical splits
    # respectively.
    bindsym $mod+b splith
    bindsym $mod+v splitv

    # Switch the current container between different layout styles
    bindsym $mod+s layout stacking
    bindsym $mod+w layout tabbed
    bindsym $mod+e layout toggle split

    # Make the current focus fullscreen
    bindsym $mod+f fullscreen

    # Toggle the current focus between tiling and floating mode
    bindsym $mod+Shift+space floating toggle

    # Swap focus between the tiling area and the floating area
    bindsym $mod+space focus mode_toggle

    # move focus to the parent container
    bindsym $mod+a focus parent
#
# Scratchpad:
#
    # Sway has a "scratchpad", which is a bag of holding for windows.
    # You can send windows there and get them back later.

    # Move the currently focused window to the scratchpad
    bindsym $mod+Shift+minus move scratchpad

    # Show the next scratchpad window or hide the focused scratchpad window.
    # If there are multiple scratchpad windows, this command cycles through them.
    bindsym $mod+minus scratchpad show
#
# Resizing containers:
#
mode "resize" {
    # left will shrink the containers width
    # right will grow the containers width
    # up will shrink the containers height
    # down will grow the containers height
    bindsym $left resize shrink width 10px
    bindsym $left+shift resize shrink width 100px
    bindsym $down resize grow height 10px
    bindsym $down+shift resize grow height 100px
    bindsym $up resize shrink height 10px
    bindsym $up+shift resize shrink height 100px
    bindsym $right resize grow width 10px
    bindsym $right+shift resize grow width 100px

    # ditto, with arrow keys
    bindsym Left resize shrink width 10px
    bindsym Left+shift resize shrink width 100px
    bindsym Down resize grow height 10px
    bindsym Down+shift resize grow height 100px
    bindsym Up resize shrink height 10px
    bindsym Up+shift resize shrink height 100px
    bindsym Right resize grow width 10px
    bindsym Right+shift resize grow width 100px

    # return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"


# Status Bar:
################################################################################
# Read `man 5 sway-bar` for more information about this section.
bar {
    position top

    # When the status_command prints a new line to stdout, swaybar updates.
	swaybar_command waybar
}

include /etc/sway/config.d/*
include ~/.config/sway/`hostname`
