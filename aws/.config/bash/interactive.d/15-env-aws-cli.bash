################################################################################
# aws-cli.bash - environment overrides for AWS CLI https://aws.amazon.com/cli/
################################################################################

# TODO: these cause profile resolution to fail: https://github.com/aws/aws-cli/issues/3304
#export AWS_CONFIG_FILE="$XDG_CONFIG_HOME/aws/config"
#export AWS_CLI_HISTORY_FILE="$XDG_DATA_HOME/aws/history"
#export AWS_CREDENTIALS_FILE="$XDG_CONFIG_HOME/aws/credentials"
#export AWS_WEB_IDENTITY_TOKEN_FILE="$XDG_CONFIG_HOME/aws/token"
#export AWS_SHARED_CREDENTIALS_FILE="$XDG_CONFIG_HOME/aws/shared-credentials"

alias aws="aws2"
# Enable command completion
# BROKEN: https://github.com/aws/aws-cli/issues/4656
#complete -C `which /usr/local/bin/aws2_completer` aws2
