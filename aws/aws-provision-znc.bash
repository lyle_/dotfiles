#!/usr/bin/env bash
#
# Provision an EC2 instance with ZNC, the IRC bouncer

# Install necessary packages
sudo yum --assumeyes update
sudo yum --assumeyes install \
	https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm \
	git \
	znc

# Create user with appropriate auth key and sudoers permissions
sudo useradd ldh
sudo mkdir ~ldh/.ssh
sudo chmod 700 ~ldh/.ssh
sudo cp ~ec2-user/.ssh/authorized_keys ~ldh/.ssh
sudo chmod 600 ~ldh/.ssh/authorized_keys
sudo chown -R ldh:ldh ~ldh/.ssh
sudo sh -c 'echo "ldh ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers'

echo "Manual steps:"
echo "$ sudo -u znc znc --makeconf"
echo "sudo systemctl enable znc"
echo "sudo systemctl start znc"
