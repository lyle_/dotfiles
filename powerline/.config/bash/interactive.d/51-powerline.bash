################################################################################
# powerline.bash - a dollar sign for $PS1 is pretty cool, but we can do better
################################################################################

# Powerline - https://wiki.archlinux.org/index.php/Powerline
if [ -f `which powerline-daemon` ]; then
    powerline-daemon -q
    POWERLINE_BASH_CONTINUATION=1
    POWERLINE_BASH_SELECT=1
fi
if [ -f /usr/local/lib/python3.8/dist-packages/powerline/bindings/bash/powerline.sh ]; then
	# Arch
    source /usr/local/lib/python3.8/dist-packages/powerline/bindings/bash/powerline.sh
elif [ -f /usr/share/powerline/bindings/bash/powerline.sh ]; then
	# Debian
	source /usr/share/powerline/bindings/bash/powerline.sh
elif [[ -f /usr/local/lib/python3.9/dist-packages/powerline/bindings/bash/powerline.sh ]]; then
	source /usr/local/lib/python3.9/dist-packages/powerline/bindings/bash/powerline.sh
fi
