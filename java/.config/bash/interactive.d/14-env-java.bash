################################################################################
# env-java.bash - settings for Java
#
# See https://wiki.archlinux.org/index.php/java for general info
################################################################################

export JAVA_HOME=$(readlink -f /usr/bin/javac | sed "s:bin/javac::")

# Use system anti-aliased fonts and make swing use the GTK look and feel
#export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel -Dswing.crossplatformlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel'
# https://wiki.archlinux.org/index.php/java#Silence_'Picked_up_JAVA_OPTIONS'_message_on_command_line
# See also _JAVA_OPTIONS set in /etc/profile.d/jre.sh
#_SILENT_JAVA_OPTIONS="$_JAVA_OPTIONS"
#unset _JAVA_OPTIONS
#alias java='java "$_SILENT_JAVA_OPTIONS"'

# Fix a known problem with non-re-parenting window managers
# See https://github.com/swaywm/sway/issues/595
export _JAVA_AWT_WM_NONREPARENTING=1
