################################################################################
# current-employer-aliases.bash - aliases for my particular work environment
################################################################################

# Docker needs to be run with sudo on servers
if command -v docker >/dev/null 2>&1 && [[ $HOSTNAME = *".doit.wisc.edu" ]]; then
	alias docker='sudo docker'
fi

# CAOS scripts
if [ -d /chub/bin ]; then
	export PATH=$PATH:/chub/bin
fi

# Last CS&E index completion
alias cse_index_complete='ssh fuschia.doit.wisc.edu grep DONE /rambo/enrollment/logs/indexer/indexer-$(date -I).log'
