#!/bin/bash

echo "Setting up dotfiles..."
set -e	# exit immediatly on failure

# Path to this script, will be used as the stow directory
STOW_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# Specify default directories if not already set
XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config}"
XDG_CACHE_HOME="${XDG_CACHE_HOME:-$HOME/.cache}"


# Tests whether a specified command ($1) is available
function is_installed {
	# we expect things to not be installed, don't quit here..
	command -v "$1" >/dev/null 2>&1
}

# If a command ($1) is available, stow a package ($2)
function if_installed {
	if is_installed "$1"; then
		stow "$2"
	fi
}

# If a file ($1) is available, stow a package ($2)
function if_file_exists {
	if [[ -f $1 ]]; then
		stow "$2"
	fi
}

# Evaluates a command ($1), prepending a specified prefix ($2)
function indent {
	eval "$1" 2>&1 > /dev/null | sed "s/^/[$2] /"
}

# Test whether stow is installed *before* we redefine it as a function
if [[ -x "$(command -v stow)" || -f $HOME/.local/bin/stow ]]; then
	stow_installed=0
else
	stow_installed=1
fi

# Stows a package ($1), handling indentation and common functionality
# The second argument, if present, is passed along to stow as an option.
function stow {
	if [[ ":$PATH:" != *":$HOME/.local/bin:"* ]]; then
		printf '!!!!!! Your $PATH is missing ~/.local/bin, you might want to add it.\n'
		PATH="$PATH:$HOME/.local/bin"
	fi
	echo
	indent "command stow -vv --restow $2 $1" "$1"
}


################################################################################
# make sure stow is available
################################################################################
if [[ $stow_installed == "1" ]]; then
	is_installed perl || { echo "ERROR: perl not found, can't install stow"; exit 1; }
	is_installed curl || { echo "ERROR: curl not found, can't install stow"; exit 1; }

	# Can't use 2.2.2, installation breaks if texi2html is not present. see
	# https://lists.gnu.org/archive/html/info-stow/2016-11/msg00004.html
	stow_version="2.2.0"
	printf "\n\n###### stow is not installed, bootstrapping\n"
	cd /tmp
	indent "curl --output stow.tar.gz https://ftp.gnu.org/gnu/stow/stow-${stow_version}.tar.gz" "curl stow"

	echo
	indent "tar -zxvf stow.tar.gz" "unpack stow"
	indent "rm -f stow.tar.gz" "remove stow tarball"

	cd stow-${stow_version}
	echo
	share_dir=${STOW_DIR}/stow/.local/share
	indent "./configure --prefix=${STOW_DIR}/stow/.local --datarootdir=$share_dir --with-pmdir=$share_dir" "configure stow"
	indent "make install" "install stow"

	# Bootstrap per https://www.gnu.org/software/stow/manual/html_node/Bootstrapping.html
	echo
	cd "$STOW_DIR"
	indent "stow/.local/bin/stow -vv --stow stow" "stow stow"
	indent "rm -fr /tmp/stow-${stow_version}" "removing stow source"
else
	printf "\nstow is already installed, skipping bootstrap\n"
fi


################################################################################
# packages to always install
################################################################################
printf "\n\n###### installing common packages\n"
stow xdg
stow ack
stow current_employer
if [[ ! -L "$HOME/.bash_profile" && -f "$HOME/.bash_profile" ]]; then
	printf "\n\n!!!!!! Found an existing ~/.bash_profile, moving to $STOW_DIR/backup"
	mkdir -p "$STOW_DIR/backup"
	echo ".*" > "$STOW_DIR/backup/.stow-local-ignore"
	mv "$HOME/.bash_profile" "$STOW_DIR/backup"
fi
if [[ ! -L "$HOME/.bashrc" && -f "$HOME/.bashrc" ]]; then
	printf "\n\n!!!!!! Found an existing ~/.bashrc, moving to $STOW_DIR/backup"
	mkdir -p "$STOW_DIR/backup"
	echo ".*" > "$STOW_DIR/backup/.stow-local-ignore"
	mv "$HOME/.bashrc" "$STOW_DIR/backup"
fi
stow bash
stow readline
stow scripts
stow editorconfig


################################################################################
# packages installed if corresponding program is detected
################################################################################
printf "\n\n###### detecting conditional packages\n"
if_installed aerc aerc
if_installed ansible ansible
if is_installed aws2; then
	stow aws
	if [ -d "$DROPBOX_DIR/sync/aws" ]; then
		aws_backup_dir=/tmp/aws
		echo "[aws] Linking aws-cli configuration to synced dropbox version"
		if [ -h "$HOME/.aws" ]; then
			echo "[aws] $HOME/.aws is an existing link, replacing"
			unlink "$HOME/.aws"
		elif [ -d "$HOME/.aws" ]; then
			echo "[aws] Moving existing .aws directory to /tmp"
			mv --force "$HOME/.aws" "$aws_backup_dir"
		fi
		ln --symbolic --force "$DROPBOX_DIR/sync/aws" "$HOME/.aws"
	fi
fi
if is_installed cataclysm; then
	if [ -d "$DROPBOX_DIR/sync/cataclysm-dda" ]; then
		cataclysm_dir="$XDG_DATA_HOME/cataclysm-dda"
		cataclysm_backup_dir=/tmp/cataclysm-dda
		echo "[cataclysm] Linking cataclysm data to synced dropbox version"
		if [ -h "$cataclysm_dir" ]; then
			echo "[cataclysm] $cataclysm_dir is an existing link, replacing"
			unlink "$cataclysm_dir"
		elif [ -d "$cataclysm_dir" ]; then
			echo "[cataclysm] Moving existing cataclysm directory to /tmp"
			mv --force "$cataclysm_dir" "$cataclysm_backup_dir"
		fi
		ln --symbolic --force "$DROPBOX_DIR/sync/cataclysm-dda" "$cataclysm_dir"
	fi
fi
if_file_exists /chub/conf/environment caos
if_installed fc-match fontconfig
if_installed firefox firefox
if_installed fzf fzf
if is_installed git; then
	mkdir --parents "$XDG_CONFIG_HOME/git"
	stow git
	git_backup_dir=/tmp/git
	if [ -f "$DROPBOX_DIR/sync/git/includes" ]; then
		echo "Linking git includes to synced dropbox version"
		mkdir --parents "$git_backup_dir"
		if [ -h "$XDG_CONFIG_HOME/git/includes" ]; then
			echo "includes is an existing link, replacing"
			unlink "$XDG_CONFIG_HOME/git/includes"
		elif [ -f "$XDG_CONFIG_HOME/git/includes" ]; then
			echo "Moving existing includes file to /tmp"
			mv --force "$XDG_CONFIG_HOME/git/includes" "$git_backup_dir"
		fi
		ln --symbolic --force "$DROPBOX_DIR/sync/git/includes" "$XDG_CONFIG_HOME/git/includes"
	fi
fi
if is_installed grails; then
	# Sync grails config
	if [ -f "$DROPBOX_DIR/config/grails/settings.groovy" ]; then
		echo "Linking Grails settings to synced dropbox version"
		grails_backup_dir=/tmp/grails
		mkdir --parents "$grails_backup_dir"
		if [ -h "$HOME/.grails/settings.groovy" ]; then
			echo "settings.groovy is an existing link, replacing"
			unlink "$HOME/.grails/settings.groovy"
		elif [ -f "$HOME/.grails/settings.groovy" ]; then
			echo "Moving existing grails settings to /tmp"
			mv --force "$HOME/.grails/settings.groovy" "$grails_backup_dir"
		fi
		ln --symbolic --force "$DROPBOX_DIR/config/grails/settings.groovy" "$HOME/.grails/settings.groovy"
	else
		echo "NO Grails found"
	fi
fi
if_installed go go
if is_installed jetbrains-toolbox; then
	# Don't do directory folding, we stow very few of the files intellij uses.
	mkdir --parents "$XDG_CONFIG_HOME/intellij"
	stow intellij "--no-folding"
	idea_backup_dir=/tmp/intellij
	# Sync datasources
	if [ -f "$DROPBOX_DIR/sync/intellij/options/dataSources.xml" ]; then
		echo "Linking intellij dataSources to synced dropbox version"
		mkdir --parents "$idea_backup_dir"
		if [ -h "$XDG_CONFIG_HOME/intellij/options/dataSources.xml" ]; then
			echo "dataSources.xml is an existing link, replacing"
			unlink "$XDG_CONFIG_HOME/intellij/options/dataSources.xml"
		elif [ -f "$XDG_CONFIG_HOME/intellij/options/dataSources.xml" ]; then
			echo "Moving existing consoles directory to /tmp"
			mv --force "$XDG_CONFIG_HOME/intellij/options/dataSources.xml" "$idea_backup_dir"
		fi
		ln --symbolic --force "$DROPBOX_DIR/sync/intellij/options/dataSources.xml" "$XDG_CONFIG_HOME/intellij/options/dataSources.xml"
	fi
	if [ -d "$DROPBOX_DIR/sync/intellij/options/dataSources" ]; then
		mkdir --parents "$idea_backup_dir"
		if [ -h "$XDG_CONFIG_HOME/intellij/options/dataSources" ]; then
			echo "dataSources is an existing link, replacing"
			unlink "$XDG_CONFIG_HOME/intellij/options/dataSources"
		elif [ -f "$XDG_CONFIG_HOME/intellij/options/dataSources" ]; then
			echo "Moving existing dataSources directory to /tmp"
			mv --force "$XDG_CONFIG_HOME/intellij/options/dataSources" "$idea_backup_dir"
		fi
		ln --symbolic --force "$DROPBOX_DIR/sync/intellij/options/dataSources" "$XDG_CONFIG_HOME/intellij/options/dataSources"
	fi
	# Sync consoles
	if [ -d "$DROPBOX_DIR/sync/intellij/consoles" ]; then
		echo "Linking intellij consoles to synced dropbox version"
		mkdir --parents "$idea_backup_dir"
		if [ -h "$XDG_CONFIG_HOME/intellij/consoles" ]; then
			echo "consoles is an existing link, replacing"
			unlink "$XDG_CONFIG_HOME/intellij/consoles"
		elif [ -d "$XDG_CONFIG_HOME/intellij/consoles" ]; then
			echo "Moving existing consoles directory to /tmp"
			mv --force "$XDG_CONFIG_HOME/intellij/consoles" "$idea_backup_dir"
		fi
		ln --symbolic --force "$DROPBOX_DIR/sync/intellij/consoles" "$XDG_CONFIG_HOME/intellij/consoles"
	fi
	# Sync scratches
	if [ -d "$DROPBOX_DIR/sync/intellij/scratches" ]; then
		echo "Linking intellij scratches to synced dropbox version"
		mkdir --parents "$idea_backup_dir"
		if [ -h "$XDG_CONFIG_HOME/intellij/scratches" ]; then
			echo "scratches is an existing link, replacing"
			unlink "$XDG_CONFIG_HOME/intellij/scratches"
		elif [ -d "$XDG_CONFIG_HOME/intellij/scratches" ]; then
			echo "Moving existing scratches directory to /tmp"
			mv --force "$XDG_CONFIG_HOME/intellij/scratches" "$idea_backup_dir"
		fi
		ln --symbolic --force "$DROPBOX_DIR/sync/intellij/scratches" "$XDG_CONFIG_HOME/intellij/scratches"
	fi
fi
if_installed java java
if_installed kpcli kpcli
if_installed latexmk latexmk
if_installed lein leiningen
if_installed mako mako
if is_installed mvn; then
	stow maven
	# Sync settings
	ln --symbolic --force "$DROPBOX_DIR/maven" "$XDG_CONFIG_HOME/maven"
	# Create link for misbehaving apps which don't use the value from settings.xml
	mkdir -p "$XDG_CACHE_HOME/maven"
	ln --symbolic --force "$XDG_CACHE_HOME/maven" "$HOME/.m2"
fi
if is_installed mpd; then
	stow mpd
	mkdir -p "$XDG_DATA_HOME/mpd/playlists"
fi
if is_installed newsboat; then
	stow newsboat
	if [ -f "$DROPBOX_DIR/config/newsboat/urls" ]; then
		rm --force --recursive "$XDG_CONFIG_HOME/newsboat/urls"
		ln --symbolic --force "$DROPBOX_DIR/config/newsboat/urls" "$XDG_CONFIG_HOME/newsboat/urls"
	fi
fi
if_installed npm npm
if ! is_installed nvim; then
	./install_nvim
fi
if_installed nvim nvim
if_installed platformio platformio
if_installed powerline powerline
if is_installed psi; then
	if [ -d "$DROPBOX_DIR/config/psi" ]; then
		# Remove existing directory and link to config in dropbox
		rm --force --recursive "$XDG_CONFIG_HOME/psi"
		ln --symbolic --force "$DROPBOX_DIR/config/psi" "$XDG_CONFIG_HOME/psi"
	fi
fi
if_installed pulseaudio pulseaudio
if_installed ranger ranger
if_installed redshift redshift
if [[ -d "$HOME/.soapuios" ]]; then
	# Set up Dropbox-synced SoapUI projects
	printf "\n[SoapUI] Moving existing ~/.soapuios directory to $soapui_backup_dir"
	if [ -h "$HOME/.soapuios" ]; then
		echo "[SoapUI] $HOME/.soapuios is an existing link, replacing"
		unlink "$HOME/.soapuios"
	elif [ -d "$HOME/.soapuios" ]; then
		soapui_backup_dir=/tmp/soapuios-backup/
		mkdir --parents "$soapui_backup_dir"
		echo "[SoapUI] Moving existing ~/.soapuios directory to $soapui_backup_dir"
		mv --force "$HOME/.soapuios" "$soapui_backup_dir"
	fi
	ln --symbolic --force "$DROPBOX_DIR/SoapUI" "$HOME/.soapuios"
fi
if is_installed ssh; then
	# Keep ssh configuration and keys in Dropbox
	ssh_backup_dir=/tmp/ssh
	if [ -d "$DROPBOX_DIR/config/ssh" ]; then
		printf "\n[ssh] Linking ssh directory to synced dropbox version\n"
		mkdir --parents "$ssh_backup_dir"
		if [ -h "$HOME/.ssh" ]; then
			echo "[ssh] $HOME/.ssh is an existing link, replacing"
			unlink "$HOME/.ssh"
		elif [ -d "$HOME/.ssh" ]; then
			echo "[ssh] Moving existing ~/.ssh directory to /tmp"
			mv --force "$HOME/.ssh" "$ssh_backup_dir"
		fi
		ln --symbolic --force "$DROPBOX_DIR/config/ssh" "$HOME/.ssh"
	fi
	stow ssh
	systemctl --user --now enable ssh-agent
fi
if_installed sxiv sxiv
if_installed sway sway
if_installed systemctl systemd
if_installed termite termite
if [ -d "$HOME/.local/bin/UnityHub" ]; then
	echo "Installing UnityHub"
	stow unity
fi
if is_installed vim; then
	stow vim
	# Install minpac if it isn't already
	minpac_dir="$XDG_CACHE_HOME/vim/pack/minpac/opt"
	if [[ ! -d "$minpac_dir" ]]; then
		echo "Installing minpac for vim plugin management"
		mkdir -p "$minpac_dir"
		(cd "$minpac_dir" && git clone https://github.com/k-takata/minpac.git)
	fi
	if is_installed pip2; then
		if ! python2 -c "import pynvim" > /dev/null 2>&1; then
			echo "Installing pynvim for Python 2"
			# NOTE: requires 'python-dev' on debian-based systems
			pip2 install --user --upgrade pynvim
		fi
	fi
	if is_installed pip3; then
		if ! python3 -c "import pynvim" > /dev/null 2>&1; then
			echo "Installing pynvim for Python 3"
			pip3 install --user --upgrade pynvim
		fi
	fi
fi
if_installed wal wal
if_installed waybar waybar
if is_installed weechat; then
	stow weechat
	# Keep weechat configuration in Dropbox
	weechat_backup_dir=/tmp/weechat
	if [ -d "$DROPBOX_DIR/sync/weechat" ]; then
		printf "\n[weechat] Linking weechat config to synced dropbox version\n"
		mkdir --parents "$weechat_backup_dir"
		if [ -h "$XDG_CONFIG_HOME/weechat" ]; then
			echo "[weechat] $XDG_CONFIG_HOME/weechat is an existing link, replacing"
			unlink "$XDG_CONFIG_HOME/weechat"
		elif [ -f "$XDG_CONFIG_HOME/weechat" ]; then
			echo "[weechat] Moving existing weechat directory to /tmp"
			mv --force "$XDG_CONFIG_HOME/weechat" "$weechat_backup_dir"
		fi
		ln --symbolic --force "$DROPBOX_DIR/sync/weechat" "$XDG_CONFIG_HOME/weechat"
	else
		echo "[weechat] No weechat config exists in $DROPBOX_DIR, not linking"
	fi

	# Install weechat-matrix
	if ! pikaur -Qs weechat-poljar-matrix-git; then
		echo "[weechat] Installing weechat-poljar-matrix-git"
		pikaur -S weechat-poljar-matrix-git
		ln -s /usr/lib/weechat/python/matrix ~/.config/weechat/python/
		ln -s /usr/lib/weechat/python/matrix.py ~/.config/weechat/python/
		ln -s /usr/lib/weechat/python/matrix.py ~/.config/weechat/python/autoload/
	fi
fi


################################################################################
# packages installed on systems running sway
################################################################################
if is_installed sway; then
	printf "\n\n###### installing sway packages\n"
	stow gtk-3.0
fi

# Ensure XDG directories exist
if [[ -n "$XDG_CACHE_HOME" && ! -d "$XDG_CACHE_HOME" ]]; then
	printf "\n\nCreating XDG_CACHE_HOME directory %s\n" "$XDG_CACHE_HOME"
	mkdir -p "$XDG_CACHE_HOME"
fi

# Update terminfo
printf "\n\n###### Updating terminfo for Termite\n"
tic -x "${STOW_DIR}/termite/.config/termite/termite.terminfo"

# Source new settings
printf "\n\n###### Sourcing new Bash environment\n"
source ~/.bash_profile

printf "\n\n###### Done!\n"
printf "\nNow simply maintain this clone with push/pull to stay in sync!\n"
printf "* Note: this script is idempotent, re-run to install newly pulled dotfiles\n\n"

printf "\n\n###### (n)vim updates:\n"
printf "* To update plugins, run ':PackUpdate' from within (n)vim\n"

