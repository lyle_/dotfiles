""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" plugins.vim - Plugin management
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Managing packages/plugins with minpac: https://github.com/k-takata/minpac
if v:version >= 800
  set packpath^=$XDG_CACHE_HOME/vim
endif

packadd minpac

if exists('*minpac#init')
  " minpac is loaded.
  call minpac#init()
  " and manages itself
  call minpac#add('k-takata/minpac', {'type': 'opt'})

  " Match wal theme
  call minpac#add('dylanaraps/wal.vim')
  " Colorize color codes
  call minpac#add('lilydjwg/colorizer')
  if has('nvim')
    " nvim doesn't support powerline yet: https://github.com/powerline/powerline/issues/1287
    call minpac#add('vim-airline/vim-airline')
  else
    call minpac#add('powerline/powerline')
  endif
  " Show git status in gutter
  call minpac#add('airblade/vim-gitgutter')

  """ Editing and Navigation
  " Intelligently reopen files at your last edit position
  call minpac#add('farmergreg/vim-lastplace')
  " Commenting plugin
  call minpac#add('tpope/vim-commentary')
  " Surround text objects
  call minpac#add('tpope/vim-surround')
  " EditorConfig
  call minpac#add('editorconfig/editorconfig-vim')
  " XML/HTML/*ML completion
  call minpac#add('tpope/vim-ragtag')
  " Full path fuzzy finding
  call minpac#add('ctrlpvim/ctrlp.vim')
  " Change working directory to project root
  call minpac#add('airblade/vim-rooter')

  """ Autocompletion and tags
  if has('python3')
    call minpac#add('Shougo/deoplete.nvim')
    call minpac#add('deoplete-plugins/deoplete-go')
  endif
  if !has('nvim')
    call minpac#add('roxma/nvim-yarp')
    call minpac#add('roxma/vim-hug-neovim-rpc')
  endif
  " Git support
  call minpac#add('tpope/vim-fugitive')

  """ Language support
  " Clojure development
  call minpac#add('tpope/vim-fireplace')
  " Go development
  call minpac#add('fatih/vim-go')
  " ALE - Asynchronous Lint Engine
  call minpac#add('w0rp/ale')
  " Markdown preview TODO: requires `:call mkdp#util#install()`
  call minpac#add('iamcco/markdown-preview.nvim')
  " Terraform
  call minpac#add('hashivim/vim-terraform')
  " Tagbar - project outline viewer
  call minpac#add('majutsushi/tagbar')

  """ Workflow
  " Vimwiki
  call minpac#add('vimwiki/vimwiki')
  call minpac#add('mhinz/vim-startify')
endif

" Define user commands for updating/cleaning the plugins.
" Each of them loads minpac, reloads .vimrc to register the
" information of plugins, then performs the task.
command! PackUpdate packadd minpac | source ~/.vim/vimrc | call minpac#update('', {'do': 'call minpac#status()'})
command! PackClean  packadd minpac | source ~/.vim/vimrc | call minpac#clean()
command! PackStatus packadd minpac | source ~/.vim/vimrc | call minpac#status()


" deoplete
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if exists('g:python_3_host_prog')
  let g:deoplete#sources#go#gocode_binary = $GOPATH.'/bin/gocode'
endif
let g:deoplete#enable_at_startup = 1


" ALE (asynchronous lint engine)
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Error and warning signs.
let g:ale_sign_error = '❌'
let g:ale_sign_warning = '⚠️'
let g:ale_sign_info = 'ℹ'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_fixers = {
\	'cpp': ['clang-format'],
\}
nmap <silent> <Leader>g <Plug>(ale_go_to_definition)


" vim-go
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" " I often can't help what version of (n)vim I'm running on remote servers
let g:go_version_warning = 0

let g:go_fmt_command = "goimports"
let g:go_autodetect_gopath = 1
let g:go_list_type = "quickfix"

let g:go_highlight_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_function_calls = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_generate_tags = 1

" Open :GoDeclsDir with ctrl-g
nmap <C-g> :GoDeclsDir<cr>
imap <C-g> <esc>:<C-u>GoDeclsDir<cr>

augroup go-settings
  autocmd!

  " This *should* by default, but for some reason I have to redefine it.
  autocmd BufWritePre *.go :call go#auto#fmt_autosave()

  " :GoBuild and :GoTestCompile
  autocmd FileType go nmap <leader>b :<C-u>call <SID>build_go_files()<CR>

  " :GoTest
  autocmd FileType go nmap <leader>t  <Plug>(go-test)

  " :GoRun
  autocmd FileType go nmap <leader>r  <Plug>(go-run)

  " :GoDoc
  autocmd FileType go nmap <Leader>d <Plug>(go-doc)

  " :GoCoverageToggle
  autocmd FileType go nmap <Leader>c <Plug>(go-coverage-toggle)

  " :GoInfo
  autocmd FileType go nmap <Leader>i <Plug>(go-info)

  " :GoMetaLinter
  autocmd FileType go nmap <Leader>l <Plug>(go-metalinter)

  " :GoDef but opens in a vertical split
  autocmd FileType go nmap <Leader>v <Plug>(go-def-vertical)
  " :GoDef but opens in a horizontal split
  autocmd FileType go nmap <Leader>s <Plug>(go-def-split)

  " :GoAlternate  commands :A, :AV, :AS and :AT
  autocmd Filetype go command! -bang A call go#alternate#Switch(<bang>0, 'edit')
  autocmd Filetype go command! -bang AV call go#alternate#Switch(<bang>0, 'vsplit')
  autocmd Filetype go command! -bang AS call go#alternate#Switch(<bang>0, 'split')
  autocmd Filetype go command! -bang AT call go#alternate#Switch(<bang>0, 'tabe')
augroup END

" build_go_files is a custom function that builds or compiles the test file.
" It calls :GoBuild if its a Go file, or :GoTestCompile if it's a test file
function! s:build_go_files()
  let l:file = expand('%')
  if l:file =~# '^\f\+_test\.go$'
    call go#test#Test(0, 1)
  elseif l:file =~# '^\f\+\.go$'
    call go#cmd#Build(0)
  endif
endfunction


" vim-terraform
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:terraform_fmt_on_save=1
let g:terraform_align=1


" vimwiki
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:vimwiki_list = [{'path': '$DROPBOX_DIR/vimwiki', 'syntax': 'markdown', 'ext': '.md'}]
" Vimwiki auto-generates tags on buffer save
let g:auto_tags = 1
" Don't default to vimwiki filetype outside of the wiki
let g:vimwiki_global_ext = 0
" Generate a level 1 header when creating a new wiki page
let g:vimwiki_auto_header = 1



" Open markdown files in browser (works well with Markdown Preview Plus)
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
autocmd BufEnter *.md execute 'nnoremap <leader>5 :!google-chrome-stable --app=file://%:p &<CR><CR>'


" CtrlP
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Ignore files in .gitignore
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']


" vim-commentary
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" For some reason Vim reads '/' as '_' in mappings
noremap <c-_> :Commentary<cr>
" Use single-line comments in C files
autocmd FileType c setlocal commentstring=//%s
autocmd FileType cpp setlocal commentstring=//%s
autocmd FileType make set commentstring=#\ %s


" vim-airline
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Show open buffers
let g:airline#extensions#tabline#enabled = 1
" Enable ALE integration
let g:airline#extensions#ale#enabled = 1
" Enable powerline font symbols
let g:airline_powerline_fonts = 1
" Don't show open buffer since we've enabled the tabline
let g:airline_section_c=''
" Remove the filetype part
let g:airline_section_x=''
" Remove the file encoding part
let g:airline_section_y=''
" Remove separators for empty sections
let g:airline_skip_empty_sections = 1
