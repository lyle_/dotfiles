""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" settings.vim - Basic, high-level settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if &compatible
	set nocompatible " Be iMproved
endif


" Core mappings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Leader <space>
let mapleader = ' '
" Local leader
let maplocalleader = ','
" Look for .exrc in directories to enable project-specific settings
set exrc

" Exit insert mode using 'jk'
inoremap jk <esc>


" Navigation
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Buffer navigation
nnoremap <C-h> :bprevious<CR>
nnoremap <C-l> :bnext<CR>

" Quickfix navigation
noremap <C-j> :cnext<CR>
noremap <C-k> :cprevious<CR>
" Close the quickfix window
nnoremap <leader>a :cclose<CR>

" Command mode navigation
cnoremap <C-k> <Up>
cnoremap <C-j> <Down>

" Splits open right and bottom (vs default of left and top)
set splitbelow
set splitright

" Terminal mode
if has('nvim')
	" Use escape to go back to normal mode
	tnoremap jk <C-\><C-n>
	tnoremap <C-v><Esc> <Esc>
	" Highlight the terminal cursor to distinguish from normal cursor
	highlight! link TermCursor Cursor
	highlight! TermCursorNC guibg=red guifg=white ctermbg=1 ctermbg=15
	" Use current neovim instance as preferred text editor.
	" This is useful for programs which launch $VISUAL to edit.
	if executable('nvr')
		let $VISUAL="nvr -cc split --remote-wait +'set bufhidden=wipe'"
	endif
endif


" Text editing
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Uppercase the current word from insert mode and carry on
	inoremap <c-u> <esc>viwU$a
" Uppercase the current word from normal mode
	nnoremap <leader><c-u> viwU$a
" Write again with 'sudo' when faced with "E45: 'readonly' option is set"
" but Vim wasn't run with sudo.
cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!


" netrw
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Store netrw history in the cache directory
let g:netrw_home=$XDG_CACHE_HOME.'/vim'
" Kill the banner
let g:netrw_banner = 0
" Use the tree view
let g:netrw_liststyle = 3
" Open files in previous window
let g:netrw_browse_split = 4
" Constrain the width
let g:netrw_winsize = 15


" Mappings to quickly edit common files
" 'e' prefixes mean 'edit'
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Reload vim configuration
nnoremap <leader>sv :source ~/.vim/vimrc<cr>
" Edit sway config
nnoremap <leader>es :edit $XDG_CONFIG_HOME/sway/config<cr>
" Edit vim plugins config
nnoremap <leader>evp :vsplit $XDG_CONFIG_HOME/vim/plugins.vim<cr>
" Edit vim general settings
nnoremap <leader>evs :vsplit $XDG_CONFIG_HOME/vim/settings.vim<cr>
" Edit vim text formatting settings
nnoremap <leader>evt :vsplit $XDG_CONFIG_HOME/vim/text-format.vim<cr>

" Use fold markers for Vimscript {{{
augroup filetype_vim
	autocmd!
	autocmd FileType vim setlocal foldmethod=marker
augroup END
" }}}

" Shell script mappings
augroup filetype_sh
	autocmd!
	autocmd FileType sh nnoremap <leader>r :!%:p<Enter>
augroup END

" Search is case-insensitive unless I explicitly use uppercase
set ignorecase
set smartcase

" Enable filetype detection, plugin loading, and indentation
filetype plugin indent on
" Enable omni completion
set omnifunc=syntaxcomplete#Complete
" Enable syntax highlighting
syntax enable
" Enable switching buffers without saving the current one
set hidden
" Write the file out when switching buffers, running :make, etc.
set autowrite
" Maintain undo history between sessions
set undofile
set undodir=${XDG_CACHE_HOME}/vim/undo

" Show line numbers
set number
if exists("+relativenumber")
	set relativenumber
endif

" Automatically save and restore folds
augroup remember_folds
	autocmd!
	autocmd BufWinLeave * silent! mkview
	autocmd BufWinEnter * silent! loadview
augroup END

" Exclude quickfix buffer from :bnext and :bprevious
augroup qf
	autocmd!
	autocmd FileType qf set nobuflisted
augroup END

" Remove whitespace on saving
autocmd BufWritePre * %s/\s\+$//e

" Set directory for 'mkview'
set viewdir=${XDG_DATA_HOME}/vim/view

" Ctags
set tags=.git/tags
