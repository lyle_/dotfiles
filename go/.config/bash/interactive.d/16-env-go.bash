################################################################################
# env-go.bash - configuration for Go development
################################################################################

export GOPATH=$XDG_LIB_HOME/go
export PATH=$PATH:/usr/local/go/bin
