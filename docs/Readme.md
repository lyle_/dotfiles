# Arch Installation and Configuration Notes

See the various documentation in this directory for specifics on configuring
various subsystems.

## Post-installation configuration

Install config files at https://gitlab.com/lyle_/Linux-Configuration.

## Auto login

Automatic login is done via overriding the getty systemd service: https://wiki.archlinux.org/index.php/Getty#Virtual_console
From there, I use bash init scripts to launch swaywm if on VT1 (~/.config/bash/login.d/*sway.bash),
and sway in turn is configured to start with the lockscreen running.
