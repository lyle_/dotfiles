# Desktop Notifications

`dunst` is used for desktop notifications, relying on `dbus` underneath.

## Problems

* If notifications fail to be sent, it could be a problem with `dbus`; as of
  `dbus-1.12.10-2`, I encountered many failures around apps requiring dbus.
  Downgrading to `1.12.10-1` has fixed the problem with dunst.
