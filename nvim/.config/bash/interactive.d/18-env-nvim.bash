################################################################################
# env-nvim.bash - variables for Neovim
################################################################################

# Avoid launching nested nvim instances from within the Neovim terminal
if [ -n "$NVIM_LISTEN_ADDRESS" ]; then
	 # If nvr (neovim-remote) is available, it uses the existing nvim instance
	 if [ -x "$(command -v nvr)" ]; then
		alias nvim=nvr
	else
		alias nvim='printf "NOPE. Starting nvim inside an nvim terminal is a bad idea.\nUse neovim-remote.\n"'
	fi
fi
