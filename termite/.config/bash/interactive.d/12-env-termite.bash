################################################################################
# env-termite.bash - environment variables for termite
################################################################################

# Enable opening a terminal in the current directory with ctrl-shift-t
if [[ $TERM == xterm-termite ]]; then
  . /etc/profile.d/vte.sh
  __vte_prompt_command
fi
